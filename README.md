## Sample: two RecyclerView inside of a NestedScrollview

Sample was built with usage of: 

1. Awesome [AdapterDelegates library](https://github.com/sockeqwe/AdapterDelegates) by **Hannes Dorfmann**
2. [Article](https://www.androiddesignpatterns.com/2018/01/experimenting-with-nested-scrolling.html) by **Alex Lockwood**

---
